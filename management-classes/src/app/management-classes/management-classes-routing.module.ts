import { NgModule } from '@angular/core';
import { ManagementClassesComponent } from './management-classes.component';
import { RouterModule, Routes } from '@angular/router';
import { AddOrEditClassComponent } from './add-or-edit-class/add-or-edit-class.component';

const routes: Routes = [
  { path: "", component: ManagementClassesComponent },
  { path: "add", component: AddOrEditClassComponent },
  { path: "edit/:id", component: AddOrEditClassComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementClassesRoutingModule { }
