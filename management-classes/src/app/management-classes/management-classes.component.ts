import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ConfirmEventType, ConfirmationService, MessageService } from "primeng/api";
import { Subscription } from "rxjs";
import { ClassService } from "../services/classes.service";
import { SummaryClassModel } from "../shared/models/class.model";

@Component({
  selector: "app-management-classes",
  templateUrl: "./management-classes.component.html",
  styleUrls: ["./management-classes.component.scss"],
  providers: [MessageService]
})

export class ManagementClassesComponent {
  tableClassSub: Subscription | undefined;

  pageIndex: number = 0;
  pageSize: number = 10;
  totalCount: number = 0;
  summaryClasses: SummaryClassModel[] = [];

  constructor(private router: Router,
              private confirmationService: ConfirmationService,
              private classService: ClassService,
              private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.classService.getClasses();
    this.tableClassSub = this.classService.getClassUpdateListener()
      .subscribe((res: {summaryClasses: SummaryClassModel[]}) => {
        this.summaryClasses = res.summaryClasses;
      });
  }

  confirm = (id: string) => {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this item??',
      header: 'Confirm',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.classService.deleteClassById(id)
        .subscribe(() => {
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Removed sucessfully!'});
          this.classService.getClasses();
        },
        error => {
          console.log(error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        });
      },
      reject: (type: ConfirmEventType) => {
        switch(type) {
          case ConfirmEventType.REJECT:
            break;
          case ConfirmEventType.CANCEL:
            break;
        }
      }
    });
  }

  redirectTo = (url: string) => {
    this.router.navigateByUrl(url);
  }

  onPageChange = (event: any) => {
    this.pageIndex = event.page;
    this.pageSize = event.rows;
  }

  ngOnDestroy(): void {
    this.tableClassSub?.unsubscribe();
  }
}
