import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ClassService } from "src/app/services/classes.service";
import { TeacherService } from "src/app/services/teachers.service";
import { ClassModel, SaveClassModel } from "src/app/shared/models/class.model";
import { DropdownModel } from "src/app/shared/models/dropdown.model";
import { StudentModel } from "src/app/shared/models/student.model";
import { TeacherModel } from "src/app/shared/models/teacher.model";
import { SelectStudentPopup } from "src/app/shared/popups/select-student.popup";

@Component({
  selector: "app-add-or-edit-class",
  templateUrl: "./add-or-edit-class.component.html",
  styleUrls: ["./add-or-edit-class.component.scss"],
  providers: [MessageService, DialogService]
})

export class AddOrEditClassComponent implements OnInit, OnDestroy {
  id: string = '';
  mode: string = '';
  title: string = '';
  isValidate: boolean = false;

  ref: DynamicDialogRef | null = null;

  pageIndex: number = 0;
  pageSize: number = 10;
  totalCount: number = 0;

  dropdownTeachers: DropdownModel[] = [];

  selectedStudents: StudentModel[] = [];

  inputFormClass = new FormGroup({
    className: new FormControl('', { validators: [Validators.required] }),
    teacher: new FormControl<DropdownModel | null>(null, { validators: [Validators.required] }),
  });

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private classService: ClassService,
    private dialogService: DialogService,
    private teacherService: TeacherService,
    private messageService: MessageService
  ) {
    this.id = this.activeRoute.snapshot.params['id'];
    this.mode = this.id ? 'edit' : 'add';
    this.title = this.mode === 'add' ? 'Create new Class' : 'Update Class';
  }

  ngOnInit(): void {
    this.teacherService.getAllTeacher().subscribe((res: any) => {
      this.dropdownTeachers = res.map((data: TeacherModel) => ({
        label: data.fullName,
        value: data.id
      }));
    });

    if (this.mode === 'edit') {
      this.classService.getClassById(this.id).subscribe((classs: ClassModel) => {
          this.selectedStudents = classs.selectedStudents;
          const selectedTeacher = this.dropdownTeachers.find((x) => x.value === classs.teacherId);
          this.inputFormClass.patchValue({
            className: classs.className,
            teacher: selectedTeacher,
          });
        }, (error) => {
          console.log(error);
          this.messageService.add({severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
          this.router.navigateByUrl('management-classes');
        }
      );
    }
  }

  saveClass = () => {
    this.isValidate = true;
    if (this.inputFormClass.invalid) {
      this.messageService.add({severity: 'error', summary: 'Error', detail: 'Please check the form input again!'});
      return;
    }

    const saveClassModel: SaveClassModel = {
      id: this.id,
      className: this.inputFormClass?.value?.className,
      teacherId: this.inputFormClass?.value?.teacher?.value,
      // teacherName: this.inputFormClass?.value?.teacher?.name,
      selectedStudents: this.selectedStudents
    };

    if (this.mode === 'add') {
      this.classService.addNewClass(saveClassModel).subscribe(() => {
          this.router.navigateByUrl('management-classes');
        }, (error) => {
          console.log(error);
          this.messageService.add({severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        }
      );
    }

    if (this.mode === 'edit') {
      this.classService.updateClass(saveClassModel).subscribe(() => {
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Update successfully!'});
          this.router.navigateByUrl('management-classes');
        }, (error) => {
          console.log(error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        }
      );
    }
  };

  cancel = () => {
    this.router.navigateByUrl('management-classes');
  };

  selectStudentPopup = () => {
    // const selectedJob = this.jobs.find(x => x.jobId === jobId)
    this.ref = this.dialogService.open(SelectStudentPopup, {
      header: 'Select students',
      width: '70%',
      contentStyle: { overflow: 'auto' },
      baseZIndex: 10000,
      maximizable: true,
      data: { selectedStudents: this.selectedStudents }
    });

    this.ref.onClose.subscribe((popupSelectedStudents: any) => {
      this.selectedStudents = popupSelectedStudents || this.selectedStudents;
    });

    // this.ref.onMaximize.subscribe((value) => {
    //     // this.messageService.add({ severity: 'info', summary: 'Maximized', detail: `maximized: ${value.maximized}` });
    // });
  }

  ngOnDestroy() {
    if (this.ref) {
        this.ref.close();
    }
 }

}
