import { NgModule } from '@angular/core';
import { ManagementClassesRoutingModule } from './management-classes-routing.module';
import { ManagementClassesComponent } from './management-classes.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimeNGModule } from '../shared/modules/primeNG.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddOrEditClassComponent } from './add-or-edit-class/add-or-edit-class.component';

@NgModule({
  declarations: [
    ManagementClassesComponent,
    AddOrEditClassComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    PrimeNGModule,
    ManagementClassesRoutingModule,
  ]
})

export class ManagementClassesModule { }
