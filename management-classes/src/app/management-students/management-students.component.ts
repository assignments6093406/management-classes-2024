import { Component, OnDestroy, OnInit } from "@angular/core";
import { StudentService } from "../services/students.service";
import { StudentModel } from "../shared/models/student.model";
import { Router } from "@angular/router";
import { ConfirmEventType, ConfirmationService, MessageService } from "primeng/api";
import { Subscription } from "rxjs";

@Component({
  selector: "app-management-students",
  templateUrl: "./management-students.component.html",
  styleUrls: ["./management-students.component.scss"],
  providers: [MessageService]
})

export class ManagementStudentsComponent implements OnInit, OnDestroy {
  tableStudentSub: Subscription | undefined;

  pageIndex: number = 0;
  pageSize: number = 10;
  totalCount: number = 0;
  students: StudentModel[] = [];

  constructor(private router: Router,
              private confirmationService: ConfirmationService,
              private studentService: StudentService,
              private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.studentService.getStudents();
    this.tableStudentSub = this.studentService.getStudentUpdateListener()
      .subscribe((res: {students: StudentModel[]}) => {
        this.students = res.students;
      });
  }

  confirm = (id: string) => {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this item??',
      header: 'Confirm',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.studentService.deleteStudentById(id)
        .subscribe(() => {
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Removed sucessfully!'});
          this.studentService.getStudents();
        },
        error => {
          console.log(error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        });
      },
      reject: (type: ConfirmEventType) => {
        switch(type) {
          case ConfirmEventType.REJECT:
            break;
          case ConfirmEventType.CANCEL:
            break;
        }
      }
    });
  }

  redirectTo = (url: string) => {
    this.router.navigateByUrl(url);
  }

  onPageChange = (event: any) => {
    this.pageIndex = event.page;
    this.pageSize = event.rows;
  }

  ngOnDestroy(): void {
    this.tableStudentSub?.unsubscribe();
  }
}
