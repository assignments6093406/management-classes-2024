import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagementStudentsComponent } from './management-students.component';
import { AddOrEditStudentComponent } from './add-or-edit-student/add-or-edit-student.component';

const routes: Routes = [
  { path: "", component: ManagementStudentsComponent },
  { path: "add", component: AddOrEditStudentComponent },
  { path: "edit/:id", component: AddOrEditStudentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementStudentsRoutingModule { }
