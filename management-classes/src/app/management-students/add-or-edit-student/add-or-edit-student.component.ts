import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { StudentService } from 'src/app/services/students.service';
import { DropdownModel } from 'src/app/shared/models/dropdown.model';
import { SaveStudentModel, StudentModel } from 'src/app/shared/models/student.model';

@Component({
  selector: 'app-add-or-edit-student',
  templateUrl: './add-or-edit-student.component.html',
  styleUrls: ['./add-or-edit-student.component.scss'],
  providers: [MessageService],
})
export class AddOrEditStudentComponent implements OnInit {
  id: string = '';
  mode: string = '';
  title: string = '';
  isValidate: boolean = false;
  dropdownGender: DropdownModel[] = [
    { label: 'Select Gender', value: null },
    { label: 'Male', value: 'Male' },
    { label: 'Female', value: 'Female' },
  ];

  inputFormStudent = new FormGroup({
    studentId: new FormControl('', { validators: [Validators.required] }),
    fullName: new FormControl('', { validators: [Validators.required] }),
    birthday: new FormControl<Date | undefined>({ value: undefined, disabled: false }, { validators: [Validators.required] }),
    address: new FormControl('', { validators: [Validators.required] }),
    gender: new FormControl<DropdownModel | null>(null, { validators: [Validators.required] }),
  });

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private studentService: StudentService,
    private messageService: MessageService
  ) {
    this.id = this.activeRoute.snapshot.params['id'];
    this.mode = this.id ? 'edit' : 'add';
    this.title = this.mode === 'add' ? 'Create new Student' : 'Update Student';

    if (this.mode === 'edit') {
      this.studentService.getStudentById(this.id).subscribe((student: StudentModel) => {
          var selectedGender = this.dropdownGender.find((x) => x.value === student.gender);
          this.inputFormStudent.patchValue({
            studentId: student.studentId,
            fullName: student.fullName,
            birthday: this.parsedDate(student.birthday?.toString()),
            address: student.address,
            gender: selectedGender,
          });
        }, (error) => {
          console.log(error);
          this.messageService.add({severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
          this.router.navigateByUrl('management-students');
        }
      );
    }
  }

  ngOnInit(): void {}

  saveStudent = () => {
    this.isValidate = true;
    if (this.inputFormStudent.invalid) {
      this.messageService.add({severity: 'error', summary: 'Error', detail: 'Please check the form input again!'});
      return;
    }

    const saveStudentModel: SaveStudentModel = {
      id: this.id,
      studentId: this.inputFormStudent?.value?.studentId,
      fullName: this.inputFormStudent?.value?.fullName,
      birthday: this.inputFormStudent?.value?.birthday,
      address: this.inputFormStudent?.value?.address,
      gender: this.inputFormStudent?.value?.gender?.value,
    };

    if (this.mode === 'add') {
      this.studentService.addNewStudent(saveStudentModel).subscribe(() => {
          this.router.navigateByUrl('management-students');
        }, (error) => {
          console.log(error);
          this.messageService.add({severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        }
      );
    }

    if (this.mode === 'edit') {
      this.studentService.updateStudent(saveStudentModel).subscribe(() => {
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Update successfully!'});
          this.router.navigateByUrl('management-students');
        }, (error) => {
          console.log(error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        }
      );
    }
  };

  cancel = () => {
    this.router.navigateByUrl('management-students');
  };

  parsedDate = (date: string | undefined): Date | undefined => {
    if (date === undefined || date === '' || date === null) {
      return undefined;
    }
    return new Date(date);
  };
}
