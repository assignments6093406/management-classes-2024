import { NgModule } from '@angular/core';
import { ManagementStudentsRoutingModule } from './management-students-routing.module';
import { ManagementStudentsComponent } from './management-students.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrimeNGModule } from '../shared/modules/primeNG.module';
import { AddOrEditStudentComponent } from './add-or-edit-student/add-or-edit-student.component';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [
    ManagementStudentsComponent,
    AddOrEditStudentComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    PrimeNGModule,
    ManagementStudentsRoutingModule,
  ],
  providers: [MessageService]
})

export class ManagementStudentsModule { }
