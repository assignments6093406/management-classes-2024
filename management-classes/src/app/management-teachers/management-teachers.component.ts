import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ConfirmEventType, ConfirmationService, MessageService } from "primeng/api";
import { Subscription } from "rxjs";
import { TeacherService } from "../services/teachers.service";
import { TeacherModel } from "../shared/models/teacher.model";

@Component({
  selector: "app-management-teachers",
  templateUrl: "./management-teachers.component.html",
  styleUrls: ["./management-teachers.component.scss"],
  providers: [MessageService]
})

export class ManagementTeachersComponent {
  tableTeacherSub: Subscription | undefined;

  pageIndex: number = 0;
  pageSize: number = 10;
  totalCount: number = 0;
  teachers: TeacherModel[] = [];

  constructor(private router: Router,
              private confirmationService: ConfirmationService,
              private teacherService: TeacherService,
              private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.teacherService.getTeachers();
    this.tableTeacherSub = this.teacherService.getTeacherUpdateListener()
      .subscribe((res: {teachers: TeacherModel[]}) => {
        this.teachers = res.teachers;
      });
  }

  confirm = (id: string) => {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this item??',
      header: 'Confirm',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.teacherService.deleteTeacherById(id)
        .subscribe(() => {
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Removed sucessfully!'});
          this.teacherService.getTeachers();
        },
        error => {
          console.log(error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        });
      },
      reject: (type: ConfirmEventType) => {
        switch(type) {
          case ConfirmEventType.REJECT:
            break;
          case ConfirmEventType.CANCEL:
            break;
        }
      }
    });
  }

  redirectTo = (url: string) => {
    this.router.navigateByUrl(url);
  }

  onPageChange = (event: any) => {
    this.pageIndex = event.page;
    this.pageSize = event.rows;
  }

  ngOnDestroy(): void {
    this.tableTeacherSub?.unsubscribe();
  }
}
