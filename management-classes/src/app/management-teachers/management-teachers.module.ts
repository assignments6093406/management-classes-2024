import { NgModule } from '@angular/core';
import { ManagementTeachersRoutingModule } from './management-teachers-routing.module';
import { ManagementTeachersComponent } from './management-teachers.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrimeNGModule } from '../shared/modules/primeNG.module';
import { AddOrEditTeacherComponent } from './add-or-edit-teacher/add-or-edit-teacher.component';

@NgModule({
  declarations: [
    ManagementTeachersComponent,
    AddOrEditTeacherComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    PrimeNGModule,
    ManagementTeachersRoutingModule,
  ]
})

export class ManagementTeachersModule { }
