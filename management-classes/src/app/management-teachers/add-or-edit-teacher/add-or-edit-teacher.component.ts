import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { TeacherService } from 'src/app/services/teachers.service';
import { DropdownModel } from 'src/app/shared/models/dropdown.model';
import { SaveTeacherModel, TeacherModel } from 'src/app/shared/models/teacher.model';

@Component({
  selector: 'app-add-or-edit-teacher',
  templateUrl: './add-or-edit-teacher.component.html',
  styleUrls: ['./add-or-edit-teacher.component.scss'],
  providers: [MessageService],
})
export class AddOrEditTeacherComponent implements OnInit {
  id: string = '';
  mode: string = '';
  title: string = '';
  isValidate: boolean = false;
  dropdownGender: DropdownModel[] = [
    { label: 'Select Gender', value: null },
    { label: 'Male', value: 'Male' },
    { label: 'Female', value: 'Female' },
  ];

  inputFormTeacher = new FormGroup({
    teacherId: new FormControl('', { validators: [Validators.required] }),
    fullName: new FormControl('', { validators: [Validators.required] }),
    birthday: new FormControl<Date | undefined>({ value: undefined, disabled: false }, { validators: [Validators.required] }),
    address: new FormControl('', { validators: [Validators.required] }),
    gender: new FormControl<DropdownModel | null>(null, { validators: [Validators.required] }),
  });

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private teacherService: TeacherService,
    private messageService: MessageService
  ) {
    this.id = this.activeRoute.snapshot.params['id'];
    this.mode = this.id ? 'edit' : 'add';
    this.title = this.mode === 'add' ? 'Create new Teacher' : 'Update Teacher';

    if (this.mode === 'edit') {
      this.teacherService.getTeacherById(this.id).subscribe((teacher: TeacherModel) => {
          var selectedGender = this.dropdownGender.find((x) => x.value === teacher.gender);
          this.inputFormTeacher.patchValue({
            teacherId: teacher.teacherId,
            fullName: teacher.fullName,
            birthday: this.parsedDate(teacher.birthday?.toString()),
            address: teacher.address,
            gender: selectedGender,
          });
        }, (error) => {
          console.log(error);
          this.messageService.add({severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
          this.router.navigateByUrl('management-teachers');
        }
      );
    }
  }

  ngOnInit(): void {}

  saveTeacher = () => {
    this.isValidate = true;
    if (this.inputFormTeacher.invalid) {
      this.messageService.add({severity: 'error', summary: 'Error', detail: 'Please check the form input again!'});
      return;
    }

    const saveTeacherModel: SaveTeacherModel = {
      id: this.id,
      teacherId: this.inputFormTeacher?.value?.teacherId,
      fullName: this.inputFormTeacher?.value?.fullName,
      birthday: this.inputFormTeacher?.value?.birthday,
      address: this.inputFormTeacher?.value?.address,
      gender: this.inputFormTeacher?.value?.gender?.value,
    };

    if (this.mode === 'add') {
      this.teacherService.addNewTeacher(saveTeacherModel).subscribe(() => {
          this.router.navigateByUrl('management-teachers');
        }, (error) => {
          console.log(error);
          this.messageService.add({severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        }
      );
    }

    if (this.mode === 'edit') {
      this.teacherService.updateTeacher(saveTeacherModel).subscribe(() => {
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Update successfully!'});
          this.router.navigateByUrl('management-teachers');
        }, (error) => {
          console.log(error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: `${error.error.error}: ${error.error.message}`});
        }
      );
    }
  };

  cancel = () => {
    this.router.navigateByUrl('management-teachers');
  };

  parsedDate = (date: string | undefined): Date | undefined => {
    if (date === undefined || date === '' || date === null) {
      return undefined;
    }
    return new Date(date);
  };
}
