import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagementTeachersComponent } from './management-teachers.component';
import { AddOrEditTeacherComponent } from './add-or-edit-teacher/add-or-edit-teacher.component';

const routes: Routes = [
  { path: "", component: ManagementTeachersComponent },
  { path: "add", component: AddOrEditTeacherComponent },
  { path: "edit/:id", component: AddOrEditTeacherComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementTeachersRoutingModule { }
