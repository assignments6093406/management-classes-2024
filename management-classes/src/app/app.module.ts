import { NO_ERRORS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrimeNGModule } from './shared/modules/primeNG.module';
import { ManagementStudentsModule } from './management-students/management-students.module';
import { ManagementClassesModule } from './management-classes/management-classes.module';
import { ManagementTeachersModule } from './management-teachers/management-teachers.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './shared/helpers/auth.interceptor';
import { MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog'
import { SelectStudentPopup } from './shared/popups/select-student.popup';

@NgModule({
  declarations: [
    AppComponent,

    SelectStudentPopup
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ManagementClassesModule,
    ManagementStudentsModule,
    ManagementTeachersModule,
    PrimeNGModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
