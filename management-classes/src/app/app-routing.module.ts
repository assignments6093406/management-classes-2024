import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagementStudentsRoutingModule } from './management-students/management-students-routing.module';
import { ManagementClassesRoutingModule } from './management-classes/management-classes-routing.module';
import { ManagementTeachersRoutingModule } from './management-teachers/management-teachers-routing.module';

const routes: Routes = [
  { path: "management-classes", loadChildren: () => ManagementClassesRoutingModule },
  { path: "management-students", loadChildren: () => ManagementStudentsRoutingModule },
  { path: "management-teachers", loadChildren: () => ManagementTeachersRoutingModule },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
