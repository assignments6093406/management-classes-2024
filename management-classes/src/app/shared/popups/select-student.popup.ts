import { Component, OnInit } from "@angular/core";
import { StudentModel } from "../models/student.model";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { StudentService } from "src/app/services/students.service";

@Component({
  template: `
  <div>
        <p-panel [header]="'Student List'" [toggleable]="true">
          <div class="grid">
              <div class="flex flex-column gap-2 col-12 md:col-6 lg:col-12">
              </div>

              <p-table [value]="students" [(selection)]="selectedStudents" dataKey="id" [tableStyle]="{'width': '100%'}">
                <ng-template pTemplate="header">
                    <tr>
                        <th style="width: 4rem">
                            <p-tableHeaderCheckbox></p-tableHeaderCheckbox>
                        </th>
                        <th>Student ID</th>
                        <th>Full Name</th>
                        <th>Gender</th>
                        <th>Address</th>
                    </tr>
                </ng-template>
                <ng-template pTemplate="body" let-student>
                    <tr>
                        <td>
                            <p-tableCheckbox [value]="student"></p-tableCheckbox>
                        </td>
                        <td>{{student.studentId}}</td>
                        <td>{{student.fullName}}</td>
                        <td>{{student.gender}}</td>
                        <td>{{student.address}}</td>
                    </tr>
                </ng-template>
            </p-table>
            </div>
        </p-panel>

        <div class="flex justify-content-end selected-student-actions">
          <p-button label="Save" class="btn-save" [disabled]="false" (click)="saveSelectedStudents()"></p-button>
          <p-button label="Cancel" class="btn-cancel" styleClass="p-button-danger" [disabled]="false"></p-button>
        </div>
    </div>
  `,
  styles: `
    :host ::ng-deep p-table {
      width: 100%;
    }
  `
})

export class SelectStudentPopup implements OnInit {

  students: StudentModel[] = [];
  selectedStudents: StudentModel[] = [];

  constructor(
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private studentService: StudentService,
  ) {
    this.selectedStudents = this.config.data.selectedStudents; // EDIT
  }

  ngOnInit(): void {
    this.studentService.getAllStudent().subscribe((students: any) => {
      this.students = students;
    })
  }

  saveSelectedStudents = () => {
    this.ref.close(this.selectedStudents);
  }

  closeDialog() {
    this.ref.close(this.selectedStudents);
  }
}
