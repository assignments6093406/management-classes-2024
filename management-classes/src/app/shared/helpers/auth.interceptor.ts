import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, switchMap, throwError } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/localStorage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  /**
   *
   */
  constructor(
    private authService: AuthService,
    private localStorageService: LocalStorageService
  ) {}

  authRequest: any;

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.localStorageService.getItem('token');

    if (token) {
      return next.handle(this.addTokenHeader(req, JSON.parse(token))).pipe(
        catchError((error) => {
          return this.handleError(req, next);
        })
      );
    }

    return next.handle(req).pipe(
      catchError((response: HttpErrorResponse) => {
        return this.handleError(req, next);
      })
    );
  }

  handleError = (req: HttpRequest<any>, next: HttpHandler) => {
    return this.authService.getToken().pipe(
      switchMap((res: any) => {
        this.authRequest = this.addTokenHeader(req, res.token);
        this.localStorageService.setItem('token', res.token);
        return next.handle(this.authRequest);
      })
    );
  };

  addTokenHeader = (request: HttpRequest<any>, token: string) => {
    return request.clone({
      headers: request.headers.set('Authorization', `Bearer ${token}`),
    });
  };
}
