
export interface TeacherModel {
  id: string; // GUID
  teacherId: string;
  fullName: string;
  birthday: Date | undefined;
  gender: string;
  address: string;
}

export interface SaveTeacherModel {
  id: string | undefined; // GUID
  teacherId: string | undefined | null;
  fullName: string | undefined | null;
  birthday: Date | undefined | null;
  gender: string | undefined | null;
  address: string | undefined | null;
}
