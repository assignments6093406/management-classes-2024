
export interface StudentModel {
  id: string; // GUID
  studentId: string;
  fullName: string;
  birthday: Date | undefined;
  gender: string;
  address: string;
}

export interface SaveStudentModel {
  id: string | undefined; // GUID
  studentId: string | undefined | null;
  fullName: string | undefined | null;
  birthday: Date | undefined | null;
  gender: string | undefined | null;
  address: string | undefined | null;
}
