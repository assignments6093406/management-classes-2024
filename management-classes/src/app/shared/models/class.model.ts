import { StudentModel } from "./student.model";

export interface ClassModel {
  id: string | undefined; // GUID
  className: string | undefined | null;
  teacherId: string | undefined | null;
  teacherName: string | undefined | null;
  // startDate: Date | undefined | null;
  // endDate: Date | undefined | null;
  selectedStudents: StudentModel[];
}

export interface SaveClassModel {
  id: string | undefined; // GUID
  className: string | undefined | null;
  teacherId: string | undefined | null;
  // teacherName: string | undefined | null;
  // startDate: Date | undefined | null;
  // endDate: Date | undefined | null;
  selectedStudents: StudentModel[];
}

export interface SummaryClassModel {
  id: string; // GUID
  className: string;
  teacherName: string;
  teacherId: string;  // GUID
  numberOfStudent: number;
}
