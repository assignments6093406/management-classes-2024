export interface DropdownModel {
  label: string | null;
  value: string | null;
}
