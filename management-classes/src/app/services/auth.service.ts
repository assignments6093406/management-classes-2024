import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

const BACKEND_URL = `${environment.baseUrl}/auth`;

@Injectable({providedIn: 'root'})
export class AuthService {

  /**
   *
   */
  constructor(private http: HttpClient) {
  }

  getToken() {
    return this.http.get(BACKEND_URL + '/getToken');
  }
}
