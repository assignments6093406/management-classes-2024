import { Injectable } from "@angular/core";


@Injectable({providedIn: 'root'})
export class LocalStorageService {
  constructor() {}
  setItem(key: string, value: any) {
    const isExisted = this.getItem(key);
    if (isExisted) {
      this.removeItem(key);
    }

    // if json, stringify it
    localStorage.setItem(key, JSON.stringify(value));
  }
  getItem(key: string): any {
    // detect and parse json later
    return localStorage.getItem(key);
  }
  removeItem(key: string) {
    localStorage.removeItem(key);
  }
  clear(): void {
    localStorage.clear();
  }
}

