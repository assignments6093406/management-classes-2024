import { Injectable } from "@angular/core";
import { SaveStudentModel, StudentModel } from "../shared/models/student.model";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

const BACKEND_URL = `${environment.baseUrl}/student`;

@Injectable({ providedIn: "root" })
export class StudentService {

  students: StudentModel[] = [];
  private tableStudentUpdated = new Subject<{students: StudentModel[]}>();

  constructor(private http: HttpClient) {
  }

  getStudentUpdateListener() {
    return this.tableStudentUpdated.asObservable();
  }

  getStudents = () => {
    return this.http.get<StudentModel[]>(BACKEND_URL + "/getStudents")
    .subscribe(res => {
      this.students = res;
      this.tableStudentUpdated.next({
        students: [...this.students]
      });
    });
  }

  getAllStudent = () => {
    return this.http.get<StudentModel[]>(BACKEND_URL + "/getStudents");
  }


  getStudentById = (id: string) => {
    return this.http.get<StudentModel>(BACKEND_URL + "/getStudentById/" + id);
  }

  addNewStudent = (student: SaveStudentModel) => {
    return this.http.post(BACKEND_URL + "/addNewStudent", student);
  }

  updateStudent = (student: SaveStudentModel) => {
    return this.http.put(BACKEND_URL + "/updateStudent", student);
  }

  deleteStudentById = (id: string) => {
    return this.http.delete(BACKEND_URL + "/deleteStudentById/" + id);
  }
}
