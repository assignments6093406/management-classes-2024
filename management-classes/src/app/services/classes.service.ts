import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ClassModel, SaveClassModel, SummaryClassModel } from "../shared/models/class.model";

const BACKEND_URL = `${environment.baseUrl}/class`;

@Injectable({ providedIn: "root" })
export class ClassService {

  summaryClasses: SummaryClassModel[] = [];
  private tableClassUpdated = new Subject<{summaryClasses: SummaryClassModel[]}>();

  constructor(private http: HttpClient) {
  }

  getClassUpdateListener() {
    return this.tableClassUpdated.asObservable();
  }

  getClasses = () => {
    return this.http.get<SummaryClassModel[]>(BACKEND_URL + "/getClasses")
    .subscribe(res => {
      this.summaryClasses = res;
      this.tableClassUpdated.next({
        summaryClasses: [...this.summaryClasses]
      });
    });;
  }

  getClassById = (id: string) => {
    return this.http.get<ClassModel>(BACKEND_URL + "/getClassById/" + id);
  }

  addNewClass = (classs: SaveClassModel) => {
    return this.http.post(BACKEND_URL + "/addNewClass", classs);
  }

  updateClass = (classs: SaveClassModel) => {
    return this.http.put(BACKEND_URL + "/updateClass", classs);
  }

  deleteClassById = (id: string) => {
    return this.http.delete(BACKEND_URL + "/deleteClassById/" + id);
  }
}
