import { Injectable } from "@angular/core";
import { SaveTeacherModel, TeacherModel } from "../shared/models/teacher.model";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

const BACKEND_URL = `${environment.baseUrl}/teacher`;

@Injectable({ providedIn: "root" })
export class TeacherService {

  teachers: TeacherModel[] = [];
  private tableTeacherUpdated = new Subject<{teachers: TeacherModel[]}>();

  constructor(private http: HttpClient) {
  }

  getTeacherUpdateListener() {
    return this.tableTeacherUpdated.asObservable();
  }

  getTeachers = () => {
    return this.http.get<TeacherModel[]>(BACKEND_URL + "/getTeachers")
    .subscribe(res => {
      this.teachers = res;
      this.tableTeacherUpdated.next({
        teachers: [...this.teachers]
      });
    });;
  }

  getAllTeacher = () => {
    return this.http.get<TeacherModel[]>(BACKEND_URL + "/getTeachers");
  }

  getTeacherById = (id: string) => {
    return this.http.get<TeacherModel>(BACKEND_URL + "/getTeacherById/" + id);
  }

  addNewTeacher = (teacher: SaveTeacherModel) => {
    return this.http.post(BACKEND_URL + "/addNewTeacher", teacher);
  }

  updateTeacher = (teacher: SaveTeacherModel) => {
    return this.http.put(BACKEND_URL + "/updateTeacher", teacher);
  }

  deleteTeacherById = (id: string) => {
    return this.http.delete(BACKEND_URL + "/deleteTeacherById/" + id);
  }
}
