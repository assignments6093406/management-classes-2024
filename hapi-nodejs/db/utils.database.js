const {Pool} = require("pg");

// Database connection configuration
const dbConfig = {
  user: "postgres",
  password: "123456",
  host: "localhost",
  port: "5432",
  database: "assignment-1",
//   ssl: true
};

var pool;

getPool_func = () => {
    if (pool) {
        return pool;
    }

    pool = new Pool(dbConfig);
    return pool;
}

query_func = (pool, query) => {
    // create new Client!!!
  return new Promise((resolve, reject) => {
    pool.query(query, (err, res) => {
      if (err) {
        console.error("Error: ", err);
        reject(err);
      }
      resolve(res);
    });
  });
};

module.exports = { getPool_func, query_func };
