// const pg = require("pg");
const {getPool_func, query_func} = require('./utils.database.js');
const { classTable } = require("./entities/class.entity.js");
const { classDetailTable } = require("./entities/classDetail.entity.js");
const { studentTable } = require("./entities/student.entity.js");
const { teacherTable } = require("./entities/teacher.entity.js");

execute = async () => {
  console.log("Starting script...");
  const client = await getPool_func().connect();

  // Entities:
  await query_func(client, studentTable);
  await query_func(client, teacherTable);
  await query_func(client, classTable);
  await query_func(client, classDetailTable);

  // SeedData:

  client.release();
};

execute().then(() => {
  console.log("Executed script.");
});
