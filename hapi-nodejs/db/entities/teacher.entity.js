const teacherTable = `
    CREATE TABLE IF NOT EXISTS public."Teacher"
    (
        "Id" uuid NOT NULL,
        "TeacherId" text COLLATE pg_catalog."default",
        "FullName" text COLLATE pg_catalog."default",
        "Birthday" date,
        "Gender" text COLLATE pg_catalog."default",
        "Address" text COLLATE pg_catalog."default",
        CONSTRAINT "Teacher_pkey" PRIMARY KEY ("Id")
    )

    TABLESPACE pg_default;

    ALTER TABLE IF EXISTS public."Teacher" OWNER to postgres;
`;

module.exports = { teacherTable };
