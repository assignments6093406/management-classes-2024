const classTable = `
    CREATE TABLE IF NOT EXISTS public."Class"
    (
        "Id" uuid NOT NULL,
        "ClassName" text COLLATE pg_catalog."default",
        "TeacherId" uuid,
        "StartDate" date,
        "EndDate" date,
        "IsCompleted" boolean default false,
        CONSTRAINT "Class_pkey" PRIMARY KEY ("Id"),
        CONSTRAINT "FK_Teacher" FOREIGN KEY ("TeacherId") REFERENCES public."Teacher" ("Id") ON DELETE CASCADE
    )

    TABLESPACE pg_default;

    ALTER TABLE IF EXISTS public."Class" OWNER to postgres;
`;

module.exports = { classTable };
