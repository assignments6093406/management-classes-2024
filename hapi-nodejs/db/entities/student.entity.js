const studentTable = `
    CREATE TABLE IF NOT EXISTS public."Student"
    (
        "Id" uuid NOT NULL,
        "StudentId" text COLLATE pg_catalog."default",
        "FullName" text COLLATE pg_catalog."default",
        "Birthday" date,
        "Gender" text COLLATE pg_catalog."default",
        "Address" text COLLATE pg_catalog."default",
        CONSTRAINT "Student_pkey" PRIMARY KEY ("Id")
    )

    TABLESPACE pg_default;

    ALTER TABLE IF EXISTS public."Student" OWNER to postgres;
`;

module.exports = { studentTable };
