const classDetailTable = `
    CREATE TABLE IF NOT EXISTS public."ClassDetail"
    (
        "Id" uuid NOT NULL,
        "ClassId" uuid NOT NULL,
        "StudentId" uuid NOT NULL,
        CONSTRAINT "ClassDetail_pkey" PRIMARY KEY ("Id"),
        CONSTRAINT "FK_Class" FOREIGN KEY ("ClassId")  REFERENCES public."Class" ("Id") ON DELETE CASCADE,
        CONSTRAINT "FK_Student" FOREIGN KEY ("StudentId")  REFERENCES public."Student" ("Id") ON DELETE CASCADE
    )

    TABLESPACE pg_default;

    ALTER TABLE IF EXISTS public."ClassDetail" OWNER to postgres;
`;

module.exports = { classDetailTable };
