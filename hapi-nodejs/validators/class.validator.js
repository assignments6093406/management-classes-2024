const joi = require('joi');

const addNewClassValidator = joi.object({
    className: joi.string().required(),
    teacherId: joi.string().guid().required(),
    selectedStudents: joi.array(),
});

const deleteClassByIdValidator = joi.object({
    id: joi.string().guid().required()
});

const getClassByIdValidator = joi.object({
    id: joi.string().guid().required()
});

const updateClassValidator = joi.object({
    id: joi.string().guid().required(),
    className: joi.string().required(),
    teacherId: joi.string().guid().required(),
    selectedStudents: joi.array(),
});

module.exports = {
    addNewClassValidator,
    deleteClassByIdValidator,
    getClassByIdValidator,
    updateClassValidator
};