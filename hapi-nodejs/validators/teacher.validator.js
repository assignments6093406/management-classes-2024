const joi = require('joi');

const addNewTeacherValidator = joi.object({
    teacherId: joi.string().required(),
    fullName: joi.string().required(),
    birthday: joi.date().required(),
    address: joi.string().required(),
    gender: joi.string().valid('Male', 'Female').required(),
});

const deleteTeacherByIdValidator = joi.object({
    id: joi.string().guid().required()
});

const getTeacherByIdValidator = joi.object({
    id: joi.string().guid().required()
});

const updateTeacherValidator = joi.object({
    id: joi.string().guid().required(),
    teacherId: joi.string().required(),
    fullName: joi.string().required(),
    birthday: joi.date().required(),
    address: joi.string().required(),
    gender: joi.string().valid('Male', 'Female').required(),
});

module.exports = {
    addNewTeacherValidator,
    deleteTeacherByIdValidator,
    getTeacherByIdValidator,
    updateTeacherValidator
};