const joi = require('joi');

const addNewStudentValidator = joi.object({
    studentId: joi.string().required(),
    fullName: joi.string().required(),
    birthday: joi.date().required(),
    address: joi.string().required(),
    gender: joi.string().valid('Male', 'Female').required(),
});

const deleteStudentByIdValidator = joi.object({
    id: joi.string().guid().required()
});

const getStudentByIdValidator = joi.object({
    id: joi.string().guid().required()
});

const updateStudentValidator = joi.object({
    id: joi.string().guid().required(),
    studentId: joi.string().required(),
    fullName: joi.string().required(),
    birthday: joi.date().required(),
    address: joi.string().required(),
    gender: joi.string().valid('Male', 'Female').required(),
});

module.exports = {
    addNewStudentValidator,
    deleteStudentByIdValidator,
    getStudentByIdValidator,
    updateStudentValidator
};