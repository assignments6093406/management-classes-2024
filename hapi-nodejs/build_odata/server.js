const { ODataServer, ODataController, ODataEntity, odata, Edm } = require('odata-v4-server');
const StudentController = require('./controllers/student.controller');

class ODataServerV1 extends ODataServer{}
ODataServerV1.define(odata.controller(StudentController, true));
ODataServerV1.create(3000);

console.log('ODataServer server listening on port: 3000');