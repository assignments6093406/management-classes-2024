const { ODataController, odata } = require("odata-v4-server");
const { createQuery } = require("odata-v4-pg");
const StudentEntity = require("../database/entities/student.entity.js");
const connect = require("../database/connect.js");

class StudentController extends ODataController {

    fakeStudents = [
        Object.assign(new StudentEntity(), {
            id: '0001',
            fullName: 'DAO NGOC ANH HOA',
            birthday: new Date(),
            gender: 'Male',
            address: 'Long An'
        }),
        Object.assign(new StudentEntity(), {
            id: '0002',
            fullName: 'NGUYEN VAN A',
            birthday: new Date(),
            gender: 'Female',
            address: 'QUAN 9'
        })
    ];

    async getStudents() {
        const db = await connect();
        // const sqlQuery = createQuery();
        const query = createQuery().from('"Student"');
        const result = await db.query(query);
        var mappingResult = result.rows.map(row => Object.assign({}, {
            id: row.Id,
            studentId: row.StudentId,
            fullName: row.FullName,
            gender: row.Gender,
            address: row.Address,
        }));
        
        return mappingResult;
    }

    getStudent(id) {
        const fakeStudents = [
            Object.assign(new StudentEntity(), {
                id: '0001',
                fullName: 'DAO NGOC ANH HOA',
                birthday: new Date(),
                gender: 'Male',
                address: 'Long An'
            }),
            Object.assign(new StudentEntity(), {
                id: '0002',
                fullName: 'NGUYEN VAN A',
                birthday: new Date(),
                gender: 'Female',
                address: 'QUAN 9'
            })
        ];

        return fakeStudents.find(function(item) { return item.id === id; });
    }

};

StudentController.define(odata.type(StudentEntity), {
    getStudents: odata.GET,
    getStudent: [odata.GET, {
        id: odata.key
    }]
});

module.exports = StudentController;