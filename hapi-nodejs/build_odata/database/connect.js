const { Client, Pool } = require("pg");

let db = null; // pg.Client

promisify = (client) => {
    return new Proxy(client, {
        get(target, name) {
            if (name !== 'query') {
                return target[name];
            }

            return function(...args) {
                return new Promise((resolve, reject) => {
                    target.query(...args, (err, result) => {
                        if (err) {
                            return reject(err);
                        }

                        resolve(result);
                    })
                });
            }
        }
    })
}

connect = () => {
  if (db) {
    return db;
  }

  const pool = new Pool({
    user: "postgres",
    password: "123456",
    host: "localhost",
    port: "5432",
    database: "assignment-1",
    //   ssl: true
  });

  return new Promise((resolve, reject) => {
    pool.connect((err, client) => {
        if (err) {
            return reject(err);
        }

        db = promisify(client);
        resolve(db);
    });
  });
}

module.exports = connect;
