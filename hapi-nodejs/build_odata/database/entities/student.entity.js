const { ODataEntity, Edm } = require("odata-v4-server");

class StudentEntity extends ODataEntity {}

StudentEntity.define({
    id: [Edm.String, Edm.Key],
    fullName: Edm.String,
    birthday: Edm.Date,
    gender: Edm.String,
    address: Edm.String,
});

module.exports = StudentEntity;
