const Jwt = require("@hapi/jwt");

module.exports = [
  {
    method: "GET",
    path: "/auth/getToken",
    config: {
        auth: false
    },
    handler: async function (request, h) {
      // TODO: Validate username and password?
      try {
        const token = Jwt.token.generate(
          {
            aud: "urn:audience:test",
            iss: "urn:issuer:test",
            user: "default_user",
            group: "testing",
          },
          {
            key: "assignment-1",
            algorithm: "HS512",
          },
          {
            ttlSec: 14400, // 4 hours
          }
        );

        return h.response({token: token});
      } catch (error) {
        console.log(error);
      }
    },
  },
];
