const auth_apis = require('./auth.route.js');
const class_apis = require('./classes.route.js');
const student_apis = require('./students.route.js');
const teacher_apis = require('./teachers.route.js');

module.exports = [].concat(auth_apis, class_apis, student_apis, teacher_apis);