const Boom = require("@hapi/boom");
const { v4: uuid_v4 } = require("uuid");

const { getPool_func, query_func } = require("../db/utils.database.js");
const {
  addNewStudentValidator,
  deleteStudentByIdValidator,
  getStudentByIdValidator,
  updateStudentValidator,
} = require("../validators/student.validator.js");

module.exports = [
  {
    method: "GET",
    path: "/student/getStudents",
    handler: async function (request, h) {
      // Pagination???
      const client = await getPool_func().connect();

      try {
        const queryString = `SELECT * FROM public."Student" student ORDER BY student."StudentId" ASC;`;
        const result = await query_func(client, queryString);
        const mappingResult = result.rows.map(data => ({
          id: data.Id,
          studentId: data.StudentId,
          fullName: data.FullName,
          gender: data.Gender,
          address: data.Address,
        }));

        return h.response(mappingResult);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "GET",
    path: "/student/getStudentById/{id}",
    handler: async function (request, h) {
      const id = request.params.id;

      const validate = getStudentByIdValidator.validate({ id: id });
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const queryString = `SELECT * FROM "Student" WHERE "Id" = '${id}' LIMIT 1`;
        const result = await query_func(client, queryString);
        if (!result || result.rows.lenght === 0) {
          return Boom.notFound(`Student ${id} does not exist in DB.`);
        }

        const mappingResult = result.rows.map(data => ({
          id: data.Id,
          studentId: data.StudentId,
          fullName: data.FullName,
          birthday: data.Birthday,
          gender: data.Gender,
          address: data.Address,
        }));

        return h.response(mappingResult[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "POST",
    path: "/student/addNewStudent",
    handler: async function (request, h) {
      const validate = addNewStudentValidator.validate(request.payload);
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_student = await query_func(client, `SELECT * FROM public."Student" student WHERE student."StudentId" = '${request.payload.studentId}';`);
        if (existed_student.rows.length > 0) {
          return Boom.badRequest(`Student ID ${request.payload.studentId} was existed in DB.`);
        }

        const queryString = `
          INSERT INTO public."Student"("Id", "StudentId", "FullName", "Birthday", "Gender", "Address")
          VALUES ('${uuid_v4()}', '${request.payload.studentId}', '${request.payload.fullName}', '${request.payload.birthday}', '${request.payload.gender}', '${request.payload.address}');
        `;

        const result = await query_func(client, queryString);

        return h.response(result.rows[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "PUT",
    path: "/student/updateStudent",
    handler: async function (request, h) {
      const validate = updateStudentValidator.validate(request.payload);
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_student = await query_func(client, `SELECT * FROM public."Student" student WHERE student."Id" = '${request.payload.id}';`);
        if (existed_student.rows.lenght === 0) {
          return Boom.notFound(`Student ID ${request.payload.studentId} does not exist in DB.`);
        }

        const queryString = `
          UPDATE public."Student"
          SET 
              "FullName"='${request.payload.fullName}',
              "Birthday"='${request.payload.birthday}',
              "Gender"='${request.payload.gender}',
              "Address"='${request.payload.address}'
          WHERE "Id" = '${request.payload.id}';
        `;
        const result = await query_func(client, queryString);

        return h.response(result.rows[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "DELETE",
    path: "/student/deleteStudentById/{id}",
    handler: async function (request, h) {
      const id = request.params.id;

      const validate = deleteStudentByIdValidator.validate({ id: id });
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_student = await query_func(client, `SELECT * FROM public."Student" student WHERE student."Id" = '${id}';`);
        if (existed_student.rows.length === 0) {
          return Boom.notFound(`Student ID ${id} does not exist in DB.`);
        }

        const queryString = `
          DELETE FROM public."Student"
          WHERE "Id" = '${id}';
        `;

        const result = await query_func(client, queryString);

        return h.response(result.rows);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },
];
