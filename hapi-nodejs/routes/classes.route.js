


const Boom = require("@hapi/boom");
const { v4: uuid_v4 } = require("uuid");
const format = require('pg-format');

const { getPool_func, query_func } = require("../db/utils.database.js");
const {
  addNewClassValidator,
  deleteClassByIdValidator,
  getClassByIdValidator,
  updateClassValidator,
} = require("../validators/class.validator.js");

module.exports = [
  {
    method: "GET",
    path: "/class/getClasses",
    handler: async function (request, h) {
      // Pagination???
      const client = await getPool_func().connect();

      try {
        const queryString = `
          SELECT classs.*, teacher."FullName" as "TeacherName", (SELECT COUNT(*) FROM "ClassDetail" classDetail WHERE classDetail."ClassId" = classs."Id") as "NumberOfStudent"
	        FROM "Class" classs
		        JOIN "Teacher" teacher ON classs."TeacherId" = teacher."Id"
          ORDER BY "ClassName" ASC
        `;

        const result = await query_func(client, queryString);
        const mappingResult = result.rows.map(data => ({
          id: data.Id,
          className: data.ClassName,
          teacherName: data.TeacherName,
          teacherId: data.TeacherId,
          numberOfStudent: data.NumberOfStudent,
        }));

        return h.response(mappingResult);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },
  
  {
    method: "GET",
    path: "/class/getClassById/{id}",
    handler: async function (request, h) {
      const id = request.params.id;

      const validate = getClassByIdValidator.validate({ id: id });
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const queryString = `
          SELECT classs.*, teacher."FullName" as "TeacherName", (SELECT COUNT(*) FROM "ClassDetail" classDetail WHERE classDetail."ClassId" = classs."Id") as "NumberOfStudent"
	        FROM "Class" classs
		        JOIN "Teacher" teacher ON classs."TeacherId" = teacher."Id"
            WHERE classs."Id" = '${id}';
        `;
        const result = await query_func(client, queryString);
        if (!result || result.rows.length === 0) {
          return Boom.notFound(`Class ID ${id} does not exist in DB.`);
        }

        const query_students = `
          SELECT student.*
	        FROM "Student" student
		        JOIN "ClassDetail" classDetail ON student."Id" = classDetail."StudentId"
            WHERE classDetail."ClassId" = '${id}';
        `;
        const result_students = await query_func(client, query_students);

        const response = {...result.rows[0], ...{selectedStudents: result_students.rows}};
        const mappingResult = {
          id: response.Id,
          className: response.ClassName,
          teacherId: response.TeacherId,
          teacherName: response.TeacherName,
          selectedStudents: response.selectedStudents.map(selectedStudent => ({
            id: selectedStudent.Id,
            studentId: selectedStudent.StudentId,
            fullName: selectedStudent.FullName,
            birthday: selectedStudent.Birthday,
            gender: selectedStudent.Gender,
            address: selectedStudent.Address,
          }))
        };

        return h.response(mappingResult);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "POST",
    path: "/class/addNewClass",
    handler: async function (request, h) {
      const validate = addNewClassValidator.validate(request.payload);
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_class = await query_func(client, `SELECT * FROM public."Class" classs WHERE classs."ClassName" = '${request.payload.className}' and classs."IsCompleted" = false;`);
        if (existed_class.rows.length > 0) {
          return Boom.badRequest(`Class ${request.payload.className} was existed in DB.`);
        }

        const existed_teacher = await query_func(client, `SELECT * FROM public."Teacher" teacher WHERE teacher."TeacherId" = '${request.payload.teacherId}';`);
        if (existed_teacher.rows.length > 0) {
          return Boom.badRequest(`Teacher ${request.payload.teacherId} does not exist in DB.`);
        }

        const classId = uuid_v4();
        const currentTime = (new Date()).toUTCString();
        const queryInsertClass = `
            INSERT INTO public."Class"("Id", "ClassName", "TeacherId", "StartDate", "EndDate", "IsCompleted")
            VALUES ('${classId}', '${request.payload.className}', '${request.payload.teacherId}', '${currentTime}', '${currentTime}', false);
        `;

        const result = await query_func(client, queryInsertClass);

        if (request.payload.selectedStudents && request.payload.selectedStudents.length > 0) {
          let values = [];
          request.payload.selectedStudents.forEach(selectedStudent => {
            values.push([uuid_v4(), classId, selectedStudent.id]);
          });

          const queryInsertClassDetail = format('INSERT INTO public."ClassDetail"("Id", "ClassId", "StudentId") VALUES %L', values);
          const classDetails = await query_func(client, queryInsertClassDetail);
        }

        return h.response(result.rows[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "PUT",
    path: "/class/updateClass",
    handler: async function (request, h) {
      const validate = updateClassValidator.validate(request.payload);
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const classId = request.payload.id;
      const client = await getPool_func().connect();

      try {
        const existed_class = await query_func(client,`SELECT * FROM public."Class" classs WHERE classs."Id" = '${classId}';`);
        if (existed_class.rows.length === 0) {
          return Boom.notFound(`Class ${classId} does not exist in DB.`);
        }

        const queryUpdateClass = `
          UPDATE public."Class"
          SET 
            "ClassName"='${request.payload.className}',
            "TeacherId"='${request.payload.teacherId}'
          WHERE "Id" = '${classId}';
        `;

        const resultUpdateClass = await query_func(client, queryUpdateClass);

        // TODO: Compare OldSelectedStudents and NewSelectedStudents
        if (request.payload.selectedStudents && request.payload.selectedStudents.length > 0) {
          const queryDeleteOldSelectedStudents = `
            DELETE FROM public."ClassDetail" classDetail WHERE classDetail."ClassId" = '${classId}';
          `;
          
          const resultDeleteOldSelectedStudents = await query_func(client, queryDeleteOldSelectedStudents);
        
          let values = [];
          request.payload.selectedStudents.forEach(selectedStudent => {
            values.push([uuid_v4(), classId, selectedStudent.id]);
          });

          const queryInsertClassDetail = format('INSERT INTO public."ClassDetail"("Id", "ClassId", "StudentId") VALUES %L', values);
          const classDetails = await query_func(client, queryInsertClassDetail);
        }

        return h.response(resultUpdateClass.rows[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "DELETE",
    path: "/class/deleteClassById/{id}",
    handler: async function (request, h) {
      const id = request.params.id;

      const validate = deleteClassByIdValidator.validate({ id: id });
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const resultClass = await query_func(client,`SELECT * FROM public."Class" classs WHERE classs."Id" = '${id}';`);
        if (resultClass.rows.length === 0) {
          return Boom.notFound(`Class ID ${id} does not exist in DB.`);
        }

        const queryDeleteClass = `
          DELETE FROM public."Class"
	        WHERE "Id" = '${id}';
        `;

        const resultDeleteClass = await query_func(client, queryDeleteClass);

        return h.response(resultDeleteClass.rows);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },
];
