const Boom = require("@hapi/boom");
const { v4: uuid_v4 } = require("uuid");

const { getPool_func, query_func } = require("../db/utils.database.js");
const {
  addNewTeacherValidator,
  deleteTeacherByIdValidator,
  getTeacherByIdValidator,
  updateTeacherValidator,
} = require("../validators/teacher.validator.js");

module.exports = [
  {
    method: "GET",
    path: "/teacher/getTeachers",
    handler: async function (request, h) {
      // Pagination???
      const client = await getPool_func().connect();

      try {
        const queryString = `SELECT * FROM public."Teacher" teacher ORDER BY teacher."TeacherId" ASC;`;
        const result = await query_func(client, queryString);
        const mappingResult = result.rows.map(data => ({
          id: data.Id,
          teacherId: data.TeacherId,
          fullName: data.FullName,
          gender: data.Gender,
          address: data.Address,
        }));

        return h.response(mappingResult);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },
  
  {
    method: "GET",
    path: "/teacher/getTeacherById/{id}",
    handler: async function (request, h) {
      const id = request.params.id;

      const validate = getTeacherByIdValidator.validate({ id: id });
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const queryString = `SELECT * FROM "Teacher" WHERE "Id" = '${id}' LIMIT 1`;
        const result = await query_func(client, queryString);
        if (!result || result.rows.lenght === 0) {
          return Boom.notFound(`Teacher ${id} does not exist in DB.`);
        }

        const mappingResult = result.rows.map(data => ({
          id: data.Id,
          teacherId: data.TeacherId,
          fullName: data.FullName,
          birthday: data.Birthday,
          gender: data.Gender,
          address: data.Address,
        }));

        return h.response(mappingResult[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "POST",
    path: "/teacher/addNewTeacher",
    handler: async function (request, h) {
      const validate = addNewTeacherValidator.validate(request.payload);
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_teacher = await query_func(client, `SELECT * FROM public."Teacher" teacher WHERE teacher."TeacherId" = '${request.payload.teacherId}';`);
        if (existed_teacher.rows.length > 0) {
          return Boom.badRequest(`Teacher ID ${request.payload.teacherId} was existed in DB.`);
        }

        const queryString = `
          INSERT INTO public."Teacher"("Id", "TeacherId", "FullName", "Birthday", "Gender", "Address")
          VALUES ('${uuid_v4()}', '${request.payload.teacherId}', '${request.payload.fullName}', '${request.payload.birthday}', '${request.payload.gender}', '${request.payload.address}');
        `;

        const result = await query_func(client, queryString);

        return h.response(result.rows[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "PUT",
    path: "/teacher/updateTeacher",
    handler: async function (request, h) {
      const validate = updateTeacherValidator.validate(request.payload);
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_teacher = await query_func(client, `SELECT * FROM public."Teacher" teacher WHERE teacher."Id" = '${request.payload.id}';`);
        if (existed_teacher.rows.lenght === 0) {
          return Boom.notFound(`Teacher ID ${request.payload.teacherId} does not exist in DB.`);
        }

        const queryString = `
          UPDATE public."Teacher"
          SET 
              "FullName"='${request.payload.fullName}',
              "Birthday"='${request.payload.birthday}',
              "Gender"='${request.payload.gender}',
              "Address"='${request.payload.address}'
          WHERE "Id" = '${request.payload.id}';
        `;
        const result = await query_func(client, queryString);

        return h.response(result.rows[0]);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },

  {
    method: "DELETE",
    path: "/teacher/deleteTeacherById/{id}",
    handler: async function (request, h) {
      const id = request.params.id;

      const validate = deleteTeacherByIdValidator.validate({ id: id });
      if (validate.error) {
        return Boom.badRequest(validate.error.message);
      }

      const client = await getPool_func().connect();

      try {
        const existed_teacher = await query_func(client, `SELECT * FROM public."Teacher" teacher WHERE teacher."Id" = '${id}';`);
        if (existed_teacher.rows.length === 0) {
          return Boom.notFound(`Teacher ID ${id} does not exist in DB.`);
        }

        const queryString = `
          DELETE FROM public."Teacher"
          WHERE "Id" = '${id}';
        `;

        const result = await query_func(client, queryString);

        return h.response(result.rows);
      } catch (error) {
        console.log(error);
      } finally {
        client.release();
      }
    },
  },
];
