const Hapi = require('@hapi/hapi');
const Jwt = require('@hapi/jwt');
const routes = require('./routes');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost',
        routes: {
            cors: true
        }
    });
   
    // Config APIs
    server.route(routes);

    await server.register(Jwt);

    server.auth.strategy('auth-1', 'jwt', {
        keys: 'assignment-1',
        verify: {
            aud: "urn:audience:test",
            iss: "urn:issuer:test",
            sub: false,
            nbf: true,
            exp: true,
            maxAgeSec: 14400, // 4 hours
            timeSkewSec: 15
        },
        validate: (artifacts, request, h) => {
            return {
                isValid: true,
                // credentials: { user: artifacts.decoded.payload.user }
            };
        }
    });

    server.auth.default('auth-1');

    await server.start();
    console.log('Server running on port 3000');
};

init();